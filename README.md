# Miniframe PHP Framework

This PHP Framework is being setup as a very lightweight Request/Response PHP framework with as few dependencies as possible,
with the goal to set up small (micro) applications in a short amount of time.

This repository contains the base for a new Miniframe PHP Framework-based project.  
You can read this readme from top to bottom, or jump to a specific section:

- [Set up your project](#set-up-your-project)
    - [Installing Docker](#installing-docker)
    - [Creating a new project with Composer](#creating-a-new-project-with-composer)
- [Start your project](#start-your-project)
- [Files in this repository](#files-in-this-repository)
- [Available middlewares](#available-middlewares)
- [Optional bundles](#optional-bundles)

## Set up your project

These steps are described using Docker.
You can also install Composer and a webserver locally, but that's not documented here.

### Installing Docker

If you already have Docker installed, you can skip this section and jump directly to [Creating a new project with Composer](#creating-a-new-project-with-composer).

* Windows installer: [Docker Desktop for Windows](https://hub.docker.com/editions/community/docker-ce-desktop-windows/).
* MacOS installer: [Docker Desktop for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac/).
* Linux: please follow the instructions to install [Docker](https://docs.docker.com/engine/install/#server) and [Docker Compose](https://docs.docker.com/compose/install/).

### Creating a new project with Composer

There is no need to download and configure this project. Composer can do this for you.  
Open a `cmd.exe` / `bash` console in an empty project folder and create a new project:

* For Windows:  
    `docker run --rm --tty --volume "%CD%:/var/www/html" garrcomm/php-apache-composer composer create-project miniframe/miniframe .`
* For Unix-based OS'es:  
    `docker run --rm --volume "${PWD}:/var/www/html" garrcomm/php-apache-composer composer create-project miniframe/miniframe .`

If you want to understand what's happening, here we'll explain this command line;

* `docker run` - we want to run a docker container.
* `--rm` - remove the container after use.
* `--tty` - For windows; use a pseudo-TTY to show colors on the console as well.
* `--volume "??:/var/www/html"` - which path must be mounted to /var/www/html inside the container
* `garrcomm/php-apache-composer` - the container to run (see [hub.docker.com](https://hub.docker.com/repository/docker/garrcomm/php-apache-composer))
  
Now follows the command that should be executed within the container;

* `composer create-project miniframe/miniframe` - Create a project based on the Composer miniframe/miniframe package
* `-s dev` tells Composer to set stability to development. This will be obsolete soon.
* `.` Tells Composer to create the project in the current folder (which we mounted with `--volume` in Docker)

## Start your project

First, we need to bring up a Docker container and enter the console.
The pre-configured container is configured in [docker-compose.yaml](docker-compose.yaml).

1. Open a `cmd.exe` / `bash` console in the project folder
2. Start the Docker container: `docker-compose up -d`
3. Open the console within the container with `docker-compose exec php bash`
4. Now all you need to do is run: `composer install`
5. You may close the console by typing: `exit` and enter.

You can now access the project at http://localhost/

When you're done, you can bring the Docker container down by running `docker-compose down`

## Files in this repository

This repository isn't large (that's the whole idea, to keep things small). It contains the following **12** files:

| Filename                   | Description                                                                           |
| -------------------------- | ------------------------------------------------------------------------------------- |
| /config/framework.ini      | Configuration of this project                                                         |
| /public/.htaccess          | Tells the Apache webserver to rewrite all unknown requests to index.php               |
| /public/index.php          | The kick starter of the framework; all requests should end up here                    |
| /src/Controller/Index.php  | The controller that's triggered for the `/` request                                   |
| /src/Controller/Readme.php | The controller that's triggered for the `/readme` request                             |
| /templates/index.html.php  | The template for the `/` request                                                      |
| /templates/readme.html.php | The template for the `/readme` request                                                |
| /.gitignore                | Tells the version control system to ignore specific files                             |
| /composer.json             | Describes the dependencies of this project and contains other metadata as well        |
| /composer.lock             | Generated file by Composer that tells which exact versions are in use of dependencies |
| /docker-compose.yaml       | Contains Docker configuration for running this application                            |
| /README.md                 | This file                                                                             |

## Available middlewares

In the `config/framework.ini`, you'll see a `middleware` directive in the configuration.
Multiple middlewares can be loaded, and are loaded in the sequence in which they are configured.

These middlewares are included in the Miniframe Core bundle and can be used to quickly set up a basic application, or as example for building your own middleware classes:

| Class name                                                                                                | Description                                                                                                                       |
| --------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| [UrlToMvcRouter](https://bitbucket.org/miniframe/core/src/v1/src/Middleware/UrlToMvcRouter.php)           | URL To MVC Router; documentation at https://bitbucket.org/miniframe/core/src/v1/src/Middleware/UrlToMvcRouter.md                  |
| [BasicAuthentication](https://bitbucket.org/miniframe/core/src/v1/src/Middleware/BasicAuthentication.php) | Basic HTTP Authentication; documentation at https://bitbucket.org/miniframe/core/src/v1/src/Middleware/BasicAuthentication.md     |
| [AccessList](https://bitbucket.org/miniframe/core/src/v1/src/Middleware/AccessList.php)                   | Deny / Allow clients based by their IP; documentation at https://bitbucket.org/miniframe/core/src/v1/src/Middleware/AccessList.md |
| [Session](https://bitbucket.org/miniframe/core/src/v1/src/Middleware/Session.php)                         | PHP Sessions; documentation at https://bitbucket.org/miniframe/core/src/v1/src/Middleware/Session.md                          |

A full list of middlewares can be found at [https://miniframe.dev/middlewares](https://miniframe.dev/middlewares)

## Optional bundles

These Miniframe bundles are optionally available through Composer:

| Bundle name                                                                      | Description                                                                                                                             |
| -------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------- |
| [miniframe/twig-bundle](https://bitbucket.org/miniframe/twig-bundle)             | Addon for using Twig templates. Documentation at https://bitbucket.org/miniframe/twig-bundle/src/v1/README.md                           |
| [miniframe/sentry-bundle](https://bitbucket.org/miniframe/sentry-bundle)         | Addon for logging errors to Sentry. Documentation at https://bitbucket.org/miniframe/sentry-bundle/src/v1/README.md                     |
| [miniframe/social-login](https://bitbucket.org/miniframe/social-login)           | Addon for authentication with several external services. Documentation at https://bitbucket.org/miniframe/social-login/src/v1/README.md |
| [miniframe/developer-toolbar](https://bitbucket.org/miniframe/developer-toolbar) | Addon for easier debugging. Documentation at https://bitbucket.org/miniframe/sentry-bundle/src/v1/README.md                             |
| [miniframe/mailer-bundle](https://bitbucket.org/miniframe/mailer-bundle)         | Addon for sending mails through PHPMailer. Documentation at https://bitbucket.org/miniframe/mailer-bundle/src/v1/README.md              |

You can install them by executing `composer require [package name]` on the console.

In the future, more external Miniframe bundles will become available through Composer.
For more about those, check out [https://miniframe.dev/](https://miniframe.dev/)
