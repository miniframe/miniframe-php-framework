<?php
/* @var $baseHref string */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hello World!</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <style type="text/css">
        code {
            background-color: #eee;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <h1>Hello World!</h1>
            <p>
                Thanks for using the Miniframe Framework.
                This PHP Framework is being setup as a very lightweight Request/Response PHP framework with as few dependencies as possible,
                with the goal to set up small (micro) applications in a short amount of time.
            </p>
            <p>
                Configuration files are in <code>/config/</code> and are in the <a href="https://en.wikipedia.org/wiki/INI_file" rel="nofollow" target="_blank">.ini file format</a>.
                All files ending on <code>.ini</code> in this folder are loaded as configuration file.
                Also, there are a few middlewares available. For more about those, see the <a href="<?= htmlspecialchars($baseHref) ?>readme">README.md</a> for more about those.
            </p>
            <p>
                By default, the <code>UrlToMvcRouter</code> middleware is loaded, which converts URLs into controller functions, with <code>index/main</code> as homepage.<br>
                This means, <code>/</code> will translate to <code>App\Controller\Index::main()</code> and <code>/readme</code> will translate to <code>App\Controller\Readme::main()</code>.<br>
                For more about this middleware, read <a href="<?= htmlspecialchars($baseHref) ?>readme/router">it's documentation</a>.
            </p>
            <p>
                Also, when in development, the <code>DevelopmentToolbar</code> middleware is loaded, which adds a bug icon to the bottom right of each page.<br>
                This toolbar can help with debugging your PHP application.
                For more about this middleware, read <a href="<?= htmlspecialchars($baseHref) ?>readme/devtoolbar">it's documentation</a>.
            </p>
            <p>
                Now have fun and play with some code.
            </p>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>
