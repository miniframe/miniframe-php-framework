<?php
/* @var $baseHref string */
/* @var $markup string */
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Readme</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <style type="text/css">
        code {
            background-color: #eee;
        }
        #content {
            white-space: pre;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <p>
                <a href="<?= htmlspecialchars($baseHref) ?>" class="btn btn-primary">🏠 Home</a>
            </p>
            <div id="content"><?= htmlspecialchars($markup) ?></div>
        </div>
    </div>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/marked@1.2.8/lib/marked.min.js" integrity="sha384-RUWfNBUC+FRAgT5WA2bWdFRNMmaxe5PgOj808eNdZN0kKuPizU86wQtCBg4X8McE" crossorigin="anonymous"></script>
<script type="text/javascript">
    with(document.getElementById('content')) {
        innerHTML = marked(innerText)
            .replace(/<table>/g, '<table class="table table-striped">') // Add Bootstrap tables
            .replace(/<a /g, '<a target="_blank" rel="nofollow" ')      // Hyperlinks in new tab, don't follow
        ;
        style.whiteSpace = 'normal';
    }
</script>
</body>
</html>
