<?php

namespace App\Controller;

use Miniframe\Core\AbstractController;
use Miniframe\Core\Response;
use Miniframe\Response\PhpResponse;

/**
 * This controller responds to all calls on `/` and `/index`
 *
 * Each public method in this controller can be accessed from `/index/[method-name]`.
 * The main() method will be used if no [method-name] is specified.
 */
class Index extends AbstractController
{
    /**
     * This method responds to `/`, `/index` and `/index/main` and should return a proper Response.
     *
     * @return Response
     */
    public function main(): Response
    {
        // The PhpResponse class extends the Response class and makes it easier to work with PHP templates.
        // There are several Response classes in the Miniframe\Response namespace.
        return new PhpResponse(__DIR__ . '/../../templates/index.html.php', [
            'baseHref' => $this->config->get('framework', 'base_href'),
        ]);
    }
}
