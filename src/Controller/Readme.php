<?php

namespace App\Controller;

use Miniframe\Core\AbstractController;
use Miniframe\Core\Response;
use Miniframe\Response\PhpResponse;

/**
 * This controller responds to all calls on `/readme`
 *
 * Each public method in this controller can be accessed from `/readme/[method-name]`.
 * The main() method will be used if no [method-name] is specified.
 */
class Readme extends AbstractController
{
    /**
     * This method responds to `/readme` and `/readme/main` and should return a proper Response.
     *
     * @return Response
     */
    public function main(): Response
    {
        $markupFile = __DIR__ . '/../../README.md';

        return new PhpResponse(__DIR__ . '/../../templates/readme.html.php', [
            'baseHref' => $this->config->get('framework', 'base_href'),
            'markup'   => file_get_contents($markupFile),
        ]);
    }

    /**
     * This method responds to `/readme/router` and should return a proper Response.
     *
     * @return Response
     */
    public function router(): Response
    {
        $markupFile = __DIR__ . '/../../vendor/miniframe/core/src/Middleware/UrlToMvcRouter.md';

        return new PhpResponse(__DIR__ . '/../../templates/readme.html.php', [
            'baseHref' => $this->config->get('framework', 'base_href'),
            'markup'   => file_get_contents($markupFile),
        ]);
    }

    /**
     * This method responds to `/readme/devtoolbar` and should return a proper Response.
     *
     * @return Response
     */
    public function devtoolbar(): Response
    {
        $markupFile = __DIR__ . '/../../vendor/miniframe/developer-toolbar/README.md';

        return new PhpResponse(__DIR__ . '/../../templates/readme.html.php', [
            'baseHref' => $this->config->get('framework', 'base_href'),
            'markup'   => file_get_contents($markupFile),
        ]);
    }
}
